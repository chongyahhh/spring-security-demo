package com.chongyahhh.security.bean.vo;

public class CommonVO {
    private String id;
    private String name;

    public CommonVO() {
    }

    public CommonVO(Object obj) {
        Object[] vars = (Object[]) obj;
        this.id = (String) vars[0];
        this.name = (String) vars[1];
    }

    public CommonVO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
