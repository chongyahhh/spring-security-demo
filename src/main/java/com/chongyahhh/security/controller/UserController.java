package com.chongyahhh.security.controller;

import com.chongyahhh.security.bean.model.BaseResult;
import com.chongyahhh.security.security.common.CommonAuthentication;
import com.chongyahhh.security.security.configs.verification.VerificationAuthentication;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/user")
public class UserController {

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public BaseResult<String> login(HttpServletRequest request){
        BaseResult<String> result = new BaseResult<>();
        CommonAuthentication user = (CommonAuthentication)SecurityContextHolder.getContext().getAuthentication();
        System.out.println(user.getUserId());
        result.construct("login success", true, null);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/wechatLogin", method = RequestMethod.POST)
    public BaseResult<String> wechatLogin(HttpServletRequest request){
        BaseResult<String> result = new BaseResult<>();
        CommonAuthentication user = (CommonAuthentication)SecurityContextHolder.getContext().getAuthentication();
        System.out.println(user.getUserId());
        result.construct("success", true, null);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/test")
    @PreAuthorize("hasPermission('/user/test', 'test')")
    public BaseResult<String> kan(HttpServletRequest request){
        BaseResult<String> result = new BaseResult<>();
        Authentication user = SecurityContextHolder.getContext().getAuthentication();
        result.construct("test access", true, null);
        return result;
    }

}
