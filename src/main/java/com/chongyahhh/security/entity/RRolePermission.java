package com.chongyahhh.security.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 角色权限关联表
 */
@Entity
@Table(name = "r_role_permission")
public class RRolePermission {
    private String id;
    private String roleId;
    private String permissionId;

    public RRolePermission() {
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", nullable = false, length = 64)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "role_id")
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Column(name = "permission_id")
    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }
}
