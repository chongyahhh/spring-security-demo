package com.chongyahhh.security.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 角色
 */
@Entity
@Table(name = "role")
public class Role {
    private String id;
    private String name;

    public Role() {
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "id", nullable = false, length = 64)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
