package com.chongyahhh.security.dao;

import com.chongyahhh.security.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDAO {
    User findByUsername(String username);
}
