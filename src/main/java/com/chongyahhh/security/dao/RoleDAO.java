package com.chongyahhh.security.dao;

import com.chongyahhh.security.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleDAO {
    List<Role> findByUsername(String username);
}
