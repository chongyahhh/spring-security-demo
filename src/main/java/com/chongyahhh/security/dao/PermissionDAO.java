package com.chongyahhh.security.dao;

import com.chongyahhh.security.entity.Permission;
import com.chongyahhh.security.security.authority.PermissionBean;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface PermissionDAO {
    Set<Permission> findByUsername(String username);

    List<PermissionBean> findRoleAndPermissionByUsername(String username);
}
