package com.chongyahhh.security.security.common;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author chongyahhh
 * 认证处理器
 * 获取所需认证信息和后处理
 */
public interface AuthenticationProcessor {

    /**
     * 预处理：获取认证所需信息
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     * @throws IOException
     * @throws ServletException
     */
    Authentication preAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException;

    /**
     * 后处理
     * @param request
     * @param response
     * @throws AuthenticationException
     * @throws IOException
     * @throws ServletException
     */
    void postAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException;
}
