package com.chongyahhh.security.security.common;

import com.chongyahhh.security.dao.PermissionDAO;
import com.chongyahhh.security.dao.RoleDAO;
import com.chongyahhh.security.dao.UserDAO;
import com.chongyahhh.security.entity.User;
import com.chongyahhh.security.security.authority.PermissionBean;
import com.chongyahhh.security.security.authority.PermissionGrantedAuthority;
import com.chongyahhh.security.security.authority.PermissionUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author chongyahhh
 * 用户认证 service
 * 为认证提供用户信息
 */
@Component
public class UserAuthenticationService {

    private final UserDAO userDAO;

    private final RoleDAO roleDAO;

    private final PermissionDAO permissionDAO;

    private final String USER_NOT_EXIST = "用户名或密码错误";

    @Autowired
    public UserAuthenticationService(UserDAO userDAO, RoleDAO roleDAO, PermissionDAO permissionDAO) {
        this.userDAO = userDAO;
        this.roleDAO = roleDAO;
        this.permissionDAO = permissionDAO;
    }

    /**
     * 在验证码认证环境下获取用户信息
     * @param username ：用户名
     * @return
     * @throws UsernameNotFoundException
     */
    public UserDetails loadUserUnderVerification(String username) throws UsernameNotFoundException {
        // 先获取用户
        User user = userDAO.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException(USER_NOT_EXIST);
        }
        // 获取权限
        Set<GrantedAuthority> authorities = new HashSet<>();
        //Set<Permission> permissions = permissionDAO.findByUsername(user.getUsername());
        List<PermissionBean> rp = permissionDAO.findRoleAndPermissionByUsername(user.getUsername());
        List<PermissionUnit> permissions = new ArrayList<>();
        PermissionGrantedAuthority role = null;
        String roleName = "";
        for(PermissionBean temp : rp){
            if(!temp.getRoleName().equals(roleName)){
                roleName = temp.getRoleName();
                if(role != null){
                    authorities.add(role);
                }
                permissions = new ArrayList<>();
                role = new PermissionGrantedAuthority(temp.getRoleName(), permissions);
            }
            permissions.add(new PermissionUnit(temp.getPermissionName(), temp.getUrl()));
        }
        authorities.add(role);
        return new CommonUser(user.getUsername(), user.getPassword(), user.getId(), authorities, user.getSalt(), user.getToken());
    }

    /**
     * 在微信登录环境下获取用户信息
     * @param openId ：用户 openId
     * @return
     * @throws UsernameNotFoundException
     */
    public UserDetails loadUserUnderWechat(String openId) throws UsernameNotFoundException {
        //
        // 通过 openId 来获取该微信绑定的本平台账号
        // 如果没有绑定的话，注册一个
        //

        return new CommonUser("admin", "123456","1", new HashSet<PermissionGrantedAuthority>(), null, null);
    }
}
