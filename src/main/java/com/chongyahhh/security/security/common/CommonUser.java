package com.chongyahhh.security.security.common;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * @Author chongyahhh
 * 用户信息
 * 为封装 Authentication 提供帮助
 */
public class CommonUser extends User {
    private String userId;
    private String salt;
    private String token;

    public CommonUser(String username, String password, Collection<? extends GrantedAuthority> authorities, String salt, String token) {
        super(username, password, authorities);
        this.salt = salt;
        this.token = token;
    }

    public CommonUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public CommonUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public CommonUser(String username, String password, String userId, Collection<? extends GrantedAuthority> authorities, String salt, String token) {
        super(username, password, authorities);
        this.salt = salt;
        this.token = token;
        this.userId = userId;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
