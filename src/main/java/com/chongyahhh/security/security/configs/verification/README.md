# spring security 验证码登录

## 验证码登录可以包含多种验证码类型，如：图片验证码、sms验证码、电子邮件等，这里特指图片验证码登录

## 如果是使用的手机验证码，不需要密码登录，可以新加 AuthenticationProvider、AuthenticationProcessor 等模块，使验证码通过后直接登录成功即可


