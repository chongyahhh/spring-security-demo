package com.chongyahhh.security.security.configs.verification;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author chongyahhh
 *
 */
@Component("tokenAuthenticationDetailsSource")
public class VerificationAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest, WebAuthenticationDetails> {

    @Override
    public WebAuthenticationDetails buildDetails(HttpServletRequest request) {
        return new VerificationWebAuthenticationDetails(request);
    }
}
