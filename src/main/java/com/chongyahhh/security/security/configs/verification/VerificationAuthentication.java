package com.chongyahhh.security.security.configs.verification;

import com.chongyahhh.security.security.common.CommonAuthentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @Author chongyahhh
 * 验证码认证对应的用户信息
 * 为了确定 provider
 */
public class VerificationAuthentication extends CommonAuthentication {
    public VerificationAuthentication(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public VerificationAuthentication(Object principal, Object credentials, String userId) {
        super(principal, credentials, userId);
    }

    public VerificationAuthentication(Object principal, Object credentials, String userId, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, userId, authorities);
    }
}
