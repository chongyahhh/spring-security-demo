package com.chongyahhh.security.security.configs.verification;

import org.springframework.security.web.authentication.WebAuthenticationDetails;

import javax.servlet.http.HttpServletRequest;

public class VerificationWebAuthenticationDetails extends WebAuthenticationDetails {
    private String verifyCode;
    private String token;

    public VerificationWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
    }
}
