package com.chongyahhh.security.security.configs.wechat;

import com.chongyahhh.security.security.common.CommonAuthentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @Author chongyahhh
 * 微信登录对应的用户信息
 * 为了确定 provider
 */
public class WechatAuthentication extends CommonAuthentication {
    public WechatAuthentication() {
        super(null, null, null);
    }

    public WechatAuthentication(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public WechatAuthentication(Object principal, Object credentials, String userId) {
        super(principal, credentials, userId);
    }

    public WechatAuthentication(Object principal, Object credentials, String userId, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, userId, authorities);
    }
}
