# spring security 微信登录

## 微信登录流程：

### 1、微信小程序端获取 code

### 2、后端通过 code、appId、appSecret 获取用户在该小程序中的 openId

### 3、后端通过 openId 确定用户信息

