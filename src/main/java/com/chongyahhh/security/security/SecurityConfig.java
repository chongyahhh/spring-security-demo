package com.chongyahhh.security.security;

import com.chongyahhh.security.security.configs.verification.VerificationLoginConfig;
import com.chongyahhh.security.security.configs.wechat.WechatLoginConfig;
import com.chongyahhh.security.security.evaluator.RolePermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;

/**
 * @Author chongyahhh
 * Spring Security 配置
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    // 匿名用户无权限异常处理入口
    private final AuthenticationEntryPoint jsonAuthenticationEntryPoint;
    // 会员用户无权限异常处理入口
    private final AccessDeniedHandler jsonAccessDeniedHandler;
    // 验证码登录配置
    private final VerificationLoginConfig verificationLoginConfig;
    // 微信登录配置
    private final WechatLoginConfig wechatLoginConfig;

    @Autowired
    public SecurityConfig(AuthenticationEntryPoint jsonAuthenticationEntryPoint, AccessDeniedHandler jsonAccessDeniedHandler, VerificationLoginConfig verificationLoginConfig, WechatLoginConfig wechatLoginConfig) {
        this.jsonAuthenticationEntryPoint = jsonAuthenticationEntryPoint;
        this.jsonAccessDeniedHandler = jsonAccessDeniedHandler;
        this.verificationLoginConfig = verificationLoginConfig;
        this.wechatLoginConfig = wechatLoginConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                    .apply(verificationLoginConfig) // 用户名密码验证码登录配置导入
                .and()
                    .apply(wechatLoginConfig) // 微信登录配置导入
                .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(jsonAuthenticationEntryPoint)
                    .accessDeniedHandler(jsonAccessDeniedHandler)
                .and()
                    .anonymous()
                .and()
                    .formLogin()
                    //.failureHandler(new JsonAuthenticationFailureHandler())
                .and()
                    .csrf().disable(); // 关闭 csrf，防止首次的 POST 请求被拦截
    }

    @Bean("customSecurityExpressionHandler")
    public DefaultWebSecurityExpressionHandler webSecurityExpressionHandler(RolePermissionEvaluator rolePermissionEvaluator){
        DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();
        handler.setPermissionEvaluator(rolePermissionEvaluator);
        return handler;
    }

}
