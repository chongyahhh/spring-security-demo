package com.chongyahhh.security.security.authority;

/**
 * @Author chongyahhh
 * 权限信息
 */
public class PermissionUnit {
    private String permission;
    private String url;

    public PermissionUnit() {
    }

    public PermissionUnit(String permission, String url) {
        this.permission = permission;
        this.url = url;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
