package com.chongyahhh.security.security.authority;

import org.springframework.security.core.GrantedAuthority;

import java.util.List;

/**
 * @Author chongyahhh
 * 权限用户信息
 */
public class PermissionGrantedAuthority implements GrantedAuthority {
    private String role;
    private List<PermissionUnit> permissions;

    public PermissionGrantedAuthority() {
    }

    public PermissionGrantedAuthority(String role, List<PermissionUnit> permissions) {
        this.role = role;
        this.permissions = permissions;
    }

    @Override
    public String getAuthority() {
        return role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<PermissionUnit> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionUnit> permissions) {
        this.permissions = permissions;
    }
}
