package com.chongyahhh.security.security.authority;

/**
 * @Author chongyahhh
 * 权限bean
 */
public class PermissionBean {
    private String roleName;
    private String permissionName;
    private String url;

    public PermissionBean() {
    }

    public PermissionBean(String roleName, String permissionName, String url) {
        this.roleName = roleName;
        this.permissionName = permissionName;
        this.url = url;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
